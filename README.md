# Web Test Automation Project

Web Test Automation Project using the framework WebDriverIO, with cucumber and selenium-standalone

## Project Configuration

Install node.js preferably v12.16.1 or higher

clone the repo

http
`git clone https://gitlab.com/ejohhnny/test-automation-project.git`

ssh
`git clone git@gitlab.com:ejohhnny/test-automation-project.git`

install the dependencies

move to the root of the project structure and instal the dependencies

`npm install`

install selenium standalone

`npx selenium-standalone install`

start selenium'standalone 

`npx selenium-standalone start`

# Execute the test cases

## run the tests locally

`npm run wdio`

## run the test in a CI/CD pipeline

go to CI/CD section, select pipelines, click en run pipeline, select branch and run

## Has a link to the results in Calliope.pro

[calliope report](https://app.calliope.pro/reports/108051)


## What you used to select the scenarios, what was your approach?

The web page was a web to verify test automation scripts, so my approach was to verify
The functionality of a few elements in the page

## Why are they the most important

They scenarios are verifying the functionaly of important elements like inputs, checkbox and radio buttons

## What could be the next steps to your project

The next steps to the project should be to use pararell execution, test the scenarios in different browsers like mozilla, safari