import Page from './page';

class ElementsPage extends Page {

    get textBoxSpan(){ return $('//span[text()="Text Box"]')}
    get checkBoxSpan(){return $('//span[text()="Check Box"]')}
    get radioButtonSpan(){return $('//span[text()="Radio Button"]')}
    get webTablesSpan(){return $('//span[text()="Web Tables"]')}
    get buttonsSpan(){return $('//span[text()="Buttons"]')}
    get linksSpan(){return $('//span[text()="Links"]')}
    get brokenLinksSpan(){return $('//span[text()="Broken Links - Images"]')}
    get uploadAndDownloadSpan(){return $('//span[text()="Upload and Download"]') }
    get dynamicPropertiesSpan(){return $('//span[text()="Dynamic Properties"]')}
    
    async clickElement(element:string){
        switch(element){
            case 'textBox':
                await this.textBoxSpan.click()
                break;
            case 'checkBox': 
                await this.checkBoxSpan.click()
                break;
            case 'radioButton':
                await this.radioButtonSpan.click()
                break;
            case 'webTables':
                await this.webTablesSpan.click()
                break;
            case 'buttons':
                await this.buttonsSpan.click()
                break;
            case 'links':
                await this.linksSpan.click()
                break;
            case 'brokenLinks':
                await this.brokenLinksSpan.click()
                break;
            case 'uploadAndDownload':
                await this.uploadAndDownloadSpan.click()
                break;
            case 'dynamicProperties':
                await this.dynamicPropertiesSpan.click()
                break;
        }
    }

    open(){
        return super.open('elements')
    }
}

export default new ElementsPage();