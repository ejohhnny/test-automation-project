Feature: Validate Elements Page visibility

Scenario: Validate TextBox Elements 
Given the user navigates to elements Page
When the user clicks textBox element in elements Page
Then the elements in textBox section should be displayed

Scenario: Validate CheckBox Elements
Given the user navigates to elements Page
When the user clicks checkBox element in elements Page
Then the elements in checkBox section should be displayed
When the user select the home checkbox
Then the result text should be displayed

Scenario: Validate Radio Button Elements
Given the user navigates to elements Page
When the user clicks radioButton element in elements Page
And the user clicks in the Yes radio button
Then the page should show the Yes text
When the user clicks in the Impressive radio button
Then the page should show the Impressive text