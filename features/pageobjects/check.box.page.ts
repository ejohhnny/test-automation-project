class CheckBoxPage {

    get homeCheckBox(){ return $('.rct-checkbox')}
    get resultText(){ return $('#result')}

    async selectHomeCheckBox(){
        await this.homeCheckBox.click()
    }

    async isHomeCheckBoxDisplayed(){
        await expect(this.homeCheckBox).toBeDisplayed()
    }

    async isResultTextDisplayed(){
        await expect(this.resultText).toBeDisplayed()
    }

    async elementShouldBeDisplayed(){
        await this.isHomeCheckBoxDisplayed()
    }

}

export default new CheckBoxPage();