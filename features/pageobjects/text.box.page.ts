class TextBoxPage {

    get fullNameInput(){ return $('#userName')}
    get emailInput(){return $('#userEmail')}
    get currentAddresInput(){return $('#currentAddress')}
    get permanetAddresInput(){return $('#permanentAddress')}

    async isFullNameInputDisplayed(){
        await expect(this.fullNameInput).toBeDisplayed()
    }

    async isEmailInputDisplayed(){
        await expect(this.emailInput).toBeDisplayed()
    }

    async isCurrentAddresInputDisplayed(){
        await expect(this.currentAddresInput).toBeDisplayed()
    }

    async isPermanetAddresInputDisplayed(){
        await expect(this.permanetAddresInput).toBeDisplayed()
    }

    async elementShouldBeDisplayed() {
        await this.isFullNameInputDisplayed()
        await this.isEmailInputDisplayed()
        await this.isCurrentAddresInputDisplayed()
        await this.isPermanetAddresInputDisplayed()
    }
}

export default new TextBoxPage();