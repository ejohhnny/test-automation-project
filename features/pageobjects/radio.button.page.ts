class RadioButtonPage {
    // I had to use this long xpath because the specific locator for the checkbox was not workin to click or
    // verify visibility o clickability
    get yesRadioButton (){ return $('//div[@class="custom-control custom-radio custom-control-inline"][1]')}
    get impressiveRadioButton() { return $('//div[@class="custom-control custom-radio custom-control-inline"][2]')}
    get noRadioButton(){ return $('input#noRadio')}
    get textSpan(){  return $('.text-success')}

    async selectYesRadioButton(){
        await this.yesRadioButton.waitForClickable({timeout:10000})
        // selecting the radio button clickin with coordinates because the function button is not working
        // in the element
        await this.yesRadioButton.click({x:10,y:5})
    }

    async selectImpresiveRadioButton(){
        await this.impressiveRadioButton.waitForClickable({timeout:10000})
        // selecting the radio button clickin with coordinates because the function button is not working
        // in the element
        await this.impressiveRadioButton.click({x:10,y:5})
    }


}

export default new RadioButtonPage();