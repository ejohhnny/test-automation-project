import { Given, When, Then } from '@wdio/cucumber-framework';

import HomePage from '../pageobjects/home.page';
import ElementsPage from '../pageobjects/elements.page';
import TextBoxPage from '../pageobjects/text.box.page';
import checkBoxPage from '../pageobjects/check.box.page';
import radioButtonPage from '../pageobjects/radio.button.page';

const pages = {
    home: HomePage,
    elements: ElementsPage,
    textBox: TextBoxPage,
    checkBox: checkBoxPage,
    radioButton: radioButtonPage
}

Given(/^the user navigates to (\w+) Page$/, async (page) => {
    await pages[page].open()
});

When(/^the user clicks (\w+) element in (\w+) Page$/, async (element, page) => {
    await pages[page].clickElement(element)
});

When(/^the user select the home checkbox$/, async () => {
    await checkBoxPage.selectHomeCheckBox()
});

When(/^the user clicks in the (\w+) radio button$/, async (radioButton) =>{
    if (radioButton == 'Yes'){
        await radioButtonPage.selectYesRadioButton()
    } else if (radioButton == 'Impressive'){
        await radioButtonPage.selectImpresiveRadioButton()
    }
    
})


Then(/^the result text should be displayed$/, async ()=> {
    await checkBoxPage.isResultTextDisplayed()
})

Then(/^the page should show the (\w+) text$/, async (text)=> {
    expect(await radioButtonPage.textSpan.getText()).toEqual(text)
})

Then(/^the elements in (\w+) section should be displayed$/, async (section) => {
    await pages[section].elementShouldBeDisplayed()
});
