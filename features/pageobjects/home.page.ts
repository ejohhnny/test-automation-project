import Page from './page';

/**
 * absctract representation of the home page
 */
class HomePage extends Page {
    /**
     * define selectors using getter methods
     */
    get elementsButton () { return $('//div[@class="card-body"]//h5[text()="Elements"]') }
    get formsButton () { return $('//div[@class="card-body"]//h5[text()="Forms"]') }
    get interactionsButton () { return $('//div[@class="card-body"]//h5[text()="Interactions"]') }
    
    /**
     *  method to go to elements section
     * 
     */
    async goToElementsSection () {
        await this.elementsButton.click();
    }

    async goToFormsSection () {
        await this.formsButton.click();
    }

    async goToInteractionsSection () {
        await this.interactionsButton.click();
    }

    /**
     * opens the home page
     */
    open () {
        return super.open();
    }
}

export default new HomePage();
